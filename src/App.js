import logo from './logo.svg';
import './App.css';
import Counter from './Counter';
import Text from './Text';
import MonthlyBal from './MonthlyBal';
import Header from './Header';
import FavoritColor from './FavoritColor';


function App() {
  return (
    <div className="App">
        <h1>Welcome to React</h1>
        {/* <Counter />
        <Text /> */}
        {/* <MonthlyBal /> */}
        <Header course="Java"/>
        <FavoritColor />
    </div>
  );
}

export default App;
