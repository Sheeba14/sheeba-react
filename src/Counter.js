import React, { Component } from 'react';

class Counter extends Component {
    constructor() {
        super();
        this.state = {
            count:0
        }
        this.changeState = this.changeState.bind(this);
    }


    changeState() {
        this.setState({
            count:this.state.count + 1
        })
    }

    render() {
        return (
            <div>
                <p>This is class component</p>
                <p>Button is clicked {this.state.count} times</p>
                <button onClick={this.changeState}>Click Here</button>
            </div>
        );
    }
}

export default Counter;