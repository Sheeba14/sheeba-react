import React, { useState } from 'react';

function FavoritColor(props) {

    const [color,setColor] = useState('');
    return (
        <div>
            <h2>My favorit Color is {color}</h2>

            <button onClick={(e)=> {setColor(e.target.innerHTML)}}>Red</button>
            <button onClick={(e)=> {setColor(e.target.innerHTML)}}>Blue</button>
            <button onClick={(e)=> {setColor(e.target.innerHTML)}}>Green</button>
            <button onClick={(e)=> {setColor(e.target.innerHTML)}}>Yellow</button>                                    
        </div>
    );
}

export default FavoritColor;