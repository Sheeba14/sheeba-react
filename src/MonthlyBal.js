import React, { Component } from 'react';

class MonthlyBal extends Component {
    constructor() {
        super();
        this.state = {
            budget:0,
            childCare:0,
            insurence:0,
            mortgage:0,
            groccery:0,
            balance:0
        }

        this.calculateBudget = this.calculateBudget.bind(this);
    }

    calculateBudget(e) {
  
        switch (e.target.id) {
            case "budget":
                this.setState({
                    budget:e.target.value
                })
                break;

            case "childCare":
                this.setState({
                    childCare:e.target.value
                })
                break;

            case "insurance":
                this.setState({
                    budget:e.target.value
                })
                break;

            case "mortgage":
                this.setState({
                    budget:e.target.value
                })
                break;

            case "groccery":
                this.setState({
                    budget:e.target.value
                })
                break;
            
            default:
                break;
        }
    this.setState({
        balance: this.state.budget - this.state.childCare - this.state.insurence - this.state.mortgage - this.state.groccery
    })
}

    render() {
        return (
            <div>
                <p>Monthly Budget</p>
                <input type="number" onChange={this.calculateBudget} id="budget"></input>
                <br></br>
                <p>Child Care</p>
                <input type="number" onChange={this.calculateBudget} id="childCare"></input>
                <br></br>
                <p>Insurance</p>
                <input type="number" onChange={this.calculateBudget} id="insurance"></input>
                <br></br>
                <p>Mortgage</p>
                <input type="number" onChange={this.calculateBudget} id="mortgage"></input>
                <br></br>
                <p>Groccery</p>
                <input type="number" onChange={this.calculateBudget} id="groccery"></input>
                <br></br>
                <p>Available Balance {this.state.budget}</p>
            </div>
        );
    }
}

export default MonthlyBal;