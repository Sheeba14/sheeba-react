import React, {useState} from 'react';

function Header(props) {

    const [color, mapColor] = useState("Blue");
    const [sport, setSport] = useState("Soccer");
    const [car, setCar] = useState({
      make:"honda",
      model:"CRV",
      year:"2018",
      color:"black"  
    })
    
    function updateCar() {
        setCar(count => {
            return {...count,model:"other"}})
    }
    
    return (
        <div>
            <h4>Coure name is {props.course}</h4>
            <h3>Favorit Color: {color}</h3>
            <button onClick={()=>mapColor("Red")}>Red Button</button>
            <h3>My car make is {car.make}, model is {car.model}, year is {car.year} and color is {car.color}</h3>
            <button onClick={updateCar}>Update</button>
        </div>
    );
}

export default Header;